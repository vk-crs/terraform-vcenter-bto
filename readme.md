# Virtual Machines on vCetner using Terraform

## Setup

* Install [terraform locally](https://learn.hashicorp.com/terraform/getting-started/install.html)
* Install Windows Server VM on VMWare vCenter with three disks attached
* Login into the Windows Server using Remote Desktop
* Navigate to Computer Configuration – Windows Settings – Security Settings – Windows Firewall with Advanced Security 
* Right click on Inbound Rules and select New Rule …
![New Inbound Rule](./assets/firewall-1.png)
* Select Predefined and choose Windows Remote Management from the drop down menu.
![Select Predefined](./assets/firewall-2.png)
* Next, uncheck the predefined rule for the public networks.
![Uncheck public networks](./assets/firewall-3.png)
* Make sure Allow the connection is selected. Click Finish.
![Allow the connection](./assets/firewall-4.png)
* Check the settings
![Check the settings](./assets/firewall-5.png)
* Execute PowerShell as an administrator to enable Ansible connectivity:
```
function Enable-WinRM(){

  $url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
  $file = "$env:temp\ConfigureRemotingForAnsible.ps1"
  (New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
  powershell.exe -ExecutionPolicy ByPass -File $file
  Enable-WSManCredSSP -Role "Server" -Force
}
 
try {
  Enable-WinRM
  exit 0;
}
Catch {
  Write-Host "An error occurred.";
  Write-Host $_;
  exit 1;
};
```
* [Create a VM Template](https://techexpert.tips/vmware/create-windows-virtual-machine-template-on-vmware-esxi/)

## Deployment

* Initialize providers: `terraform init`
* Validate configuration: `terraform validate`
* Update variables as needed in `terraform.tfvars`
* Export env vars for: `TF_VAR_vsphere_user` and `TF_VAR_vsphere_password`
* Preview changes:
   - PowerShell: `terraform plan --var-file=terraform.tfvars`
* Create/Update an environment:
   - PowerShell: `terraform apply --var-file=terraform.tfvars --auto-approve`
* Destroy the environment:
  - PowerShell: `terraform destroy --var-file=terraform.tfvars --auto-approve`
